import {getAllCarts} from '../services/cartService.js'


const fetchData = async () => {
    try {
        const results = await getAllCarts()
        return results
    } catch (error) {

    }
}

export {fetchData}