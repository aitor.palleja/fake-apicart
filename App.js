import {fetchData} from './controller/controller.js'

let arrayCarts = []
fetchData()
.then(res => {
    arrayCarts = JSON.parse(JSON.stringify(res))
    console.log("\n Primer producto: \n")
    showProductByUserId(8); //Numero de usuario
})

const showProductByUserId = (userID) =>{
    arrayCarts.forEach(element => {
        if (element.userId === userID) {
            console.log(element.products[0])
        }
        else
            console.log("Not Found");
    });
}