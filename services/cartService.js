import axios from 'axios';

const getAllCarts = async () => {
    //Request API
    return axios.get('https://fakestoreapi.com/carts')
    .then(function (response) {
        //handle success
        return response.data;
    })
    .catch(function (error) {
        //handle error
        console.log(error);
}) 
}

export {getAllCarts}

//Hacer post put y delete

